RM 	=	rm -f
all: _CodeSequentiel _Cuda _PVM _OpenMp
rebuild: clean all


_CodeSequentiel:
	cd CodeSequentiel && $(MAKE); cd ..

_Cuda:
	cd Parallel/CUDA && $(MAKE); cd ../..

_PVM:
	cd Parallel/PVM && $(MAKE); cd ../..

_OpenMp:
	cd Parallel/OpenMP && $(MAKE); cd ../..

clean:
	cd CodeSequentiel && $(MAKE) clean; cd ..
	cd Parallel/CUDA && $(MAKE) clean; cd ..
	cd Parallel/PVM && $(MAKE) clean; cd ..
	cd Parallel/OpenMP && $(MAKE) clean; cd ../..

