/*==============================================================================*/
/* Programme 	: LanceTaches.c	(Maitre)					*/
/* Auteur 	: Kevin Salaun							*/
/* Date 	: Novembre 2019							*/
/* 										*/
/* Objectifs	: Programme principal permettant de lancer 1 tache par		*/
/*		  host de la machine virtuelle, le nombre de machine a   	*/
/*		  utiliser ainsi que la taille des buffers sont paramétrables	*/
/* 										*/
/* Principe	: Ce programme se base sur un calcul distribue sur des machines	*/
/*		  PVM. 								*/
/* 										*/
/* Fonctionnemnt: 1) Analyse des hosts de la machine virtuelle			*/
/*		  2) Lancement de 1 tache par host 	 			*/
/*		  3) Liste des taches de la machine PVM	 			*/
/*==============================================================================*/


#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "pvm3.h"

#define RESULT_TAG 1
#define MAX_LENGTH 128

#define MAX_HOSTS 100
#define MAX_CHAINE 100
#define MAX_PARAM 10

#define MAITRE_ENVOI 	0
#define MAITRE_RECOIT	5

#define ESCLAVE_ENVOI	MAITRE_RECOIT
#define ESCLAVE_RECOIT 	MAITRE_ENVOI

#define CALLOC(ptr, nr, type) 		if (!(ptr = (type *) calloc((size_t)(nr), sizeof(type)))) {		\
						printf("Erreur lors de l'allocation memoire \n") ; 		\
						exit (-1);							\
					} 


#define FOPEN(fich,fichier,sens) 	if ((fich=fopen(fichier,sens)) == NULL) { 				\
						printf("Probleme d'ouverture du fichier %s\n",fichier);		\
						exit(-1);							\
					} 
					
#define MIN(a, b) 	(a < b ? a : b)
#define MAX(a, b) 	(a > b ? a : b)

#define MAX_VALEUR 	255
#define MIN_VALEUR 	0

#define NBPOINTSPARLIGNES 221

#define false 0
#define true 1
#define boolean int

int name2idx(char *name, int nhost, struct pvmhostinfo *hostp) 
{
	int f;
	for (f=0; f < nhost; f++)
	{
	   if (strcmp(name, hostp[f].hi_name) == 0)
	   	return f;
	}
	return -1;
}

main(argc, argv)
int argc;
char *argv[];
{
	/*========================================================================*/
	/* Declaration de variables et allocation memoire */
	/*========================================================================*/

	int i, j, n;
	int nbhost, nbtaches, nbarch;
	int msgtype;
	
	int info;
	int nhosttotal;
	int nhost = -1;
	int mytid;
	int tidEsclave;
	int who, retour;
	
	int LE_MIN = MAX_VALEUR;
	int LE_MAX = MIN_VALEUR;
	
	float ETALEMENT = 0.0;
	
	int **image;
	int **resultat;
	int *ligneRes;
	int X, Y, x, y;
	int TailleImage;

	int NbResultats, quelle_ligne, lignes;
	int *la_ligne;
	
	int P;
	
	FILE *Src, *Dst; //*TimeCSV;

	char SrcFile[MAX_CHAINE];
	SrcFile[0] = '\0';
	char DstFile[MAX_CHAINE];
	//char TimeCSVFile[MAX_CHAINE];
	
	char ligne[MAX_CHAINE];
	
	int NumLigne, NumTache;
	int ReponsesRecues;
	int block_size = 5000;
	
	boolean fin;
	boolean inverse = false;

	int narch;
	struct pvmhostinfo *hostp;
	struct pvmtaskinfo *taskinfo;
	int numtaches[MAX_HOSTS];
	char *param[MAX_PARAM];

	char *Chemin;
	char *CheminTache;
	char Fichier[MAX_CHAINE];

	struct timespec start_calculating_t, end_calculating_t;
	struct timespec start_global_t, end_global_t;
	double total_calculating_t = 0.0, total_global_t;

	clock_gettime(CLOCK_REALTIME, &start_global_t);
	
	/*========================================================================*/
	/* Recuperation des parametres						*/
	/*========================================================================*/

	int opt;
	while ((opt = getopt(argc, argv, "f:b:m:")) != -1) {
		switch (opt) {
			case 'f': sscanf(optarg, "%s", SrcFile); break;
			case 'b': block_size = atoi(optarg); break;
			case 'm': nhost = atoi(optarg); break;
			default:
				fprintf(stderr, "Usage: %s [-f file] [-b buffer_size] [-m machine_count]\n", argv[0]);
				exit(EXIT_FAILURE);
		}
	}

	//printf("SrcFile: %s\n", SrcFile);
	//printf("block_size: %i\n", block_size);
	//printf("nhost: %i\n", nhost);

	if(SrcFile[0] == '\0') {
		printf("No file specified\n");
		exit(EXIT_FAILURE);
	}

	//sscanf(argv[1],"%s", SrcFile);
	
	/*if (argc > 2) {
		char *a = argv[2];
		block_size = atoi(a);
	}*/
	//printf("block_size = %d\n\n", block_size);
	
	sprintf(DstFile, "%s.new", SrcFile);
	//sprintf(TimeCSVFile, "%s.cpu_time.csv", SrcFile);
	
	/*========================================================================*/
	/* Recuperation de l'endroit ou l'on travail				*/
	/*========================================================================*/

	CALLOC(Chemin, MAX_CHAINE, char);
	CALLOC(CheminTache, MAX_CHAINE, char);
	Chemin = getenv("PWD");
	//printf("Repertoire de travail : %s \n\n",Chemin);
	
	/*========================================================================*/
	/* Ouverture des fichiers						*/
	/*========================================================================*/

	//printf("Operations sur les fichiers\n");

	FOPEN(Src, SrcFile, "r");
	//printf("\t Fichier source ouvert (%s) \n",SrcFile);
		
	FOPEN(Dst, DstFile, "w");
	//printf("\t Fichier destination ouvert (%s) \n",DstFile);

	//FOPEN(TimeCSV, TimeCSVFile, "w"); // or a+
	
	/*========================================================================*/
	/* On effectue la lecture du fichier source */
	/*========================================================================*/
	
	//printf("\t Lecture entete du fichier source ");
	
	for (i = 0 ; i < 2 ; i++) {
		fgets(ligne, MAX_CHAINE, Src);	
		fprintf(Dst,"%s", ligne);
	}	

	fscanf(Src," %d %d\n",&X, &Y);
	fprintf(Dst," %d %d\n", X, Y);
	
	fgets(ligne, MAX_CHAINE, Src);	/* Lecture du 255 	*/
	fprintf(Dst,"%s", ligne);
	
	//printf(": OK \n");
	//printf("%s", ligne);
	
	
	/*========================================================================*/
	/* Allocation memoire pour l'image source et l'image resultat 		*/
	/*========================================================================*/
	CALLOC(image, Y+1, int *);
	CALLOC(resultat, Y+1, int *);
	CALLOC(ligneRes, X+1, int);
	
	for (i=0;i<Y;i++) {
		CALLOC(image[i], X+1, int);
		CALLOC(resultat[i], X+1, int);
		for (j=0;j<X;j++) {
			image[i][j] = 0;
			resultat[i][j] = 0;
		}
	}
	//printf("\t\t Initialisation de l'image [%d ; %d] : Ok \n", X, Y);
			
	TailleImage = X * Y;
	
	x = 0;
	y = 0;
	
	lignes = 0;
	
	/*========================================================================*/
	/* Lecture du fichier pour remplir l'image source 			*/
	/*========================================================================*/
	
	while (! feof(Src)) {
		n = fscanf(Src,"%d",&P);
		image[y][x] = P;	
		LE_MIN = MIN(LE_MIN, P);
		LE_MAX = MAX(LE_MAX, P);
		x ++;
		if (n == EOF || (x == X && y == Y-1)) {
			break;
		}
		if (x == X) {
			x = 0 ;
			y++;
		}
	}
	fclose(Src);
	//printf("\t Lecture du fichier image : Ok \n\n");
	
	/*========================================================================*/
	/* Calcul du facteur d'etalement					*/
	/*========================================================================*/
	
	if (inverse) {
		ETALEMENT = 0.2;	
	} else {
		ETALEMENT = (float)(MAX_VALEUR - MIN_VALEUR) / (float)(LE_MAX - LE_MIN);	
	}
	
	
	//sprintf(Fichier, "%s/Tache",*Chemin);
	sprintf(Fichier, "./Tache");

	hostp = calloc(1, sizeof(struct pvmhostinfo));
	taskinfo = calloc(1, sizeof(struct pvmtaskinfo));
	
	int iddaemon ;

	int valeur;

	mytid = pvm_mytid();


	info = pvm_tasks(0, &nbtaches, &taskinfo);
	//printf("\nListe des taches de la Parallel Virtuelle Machine \n");
	for (i=0 ; i < nbtaches ; i++) {
		if (taskinfo[i].ti_tid == mytid) {
			//printf("\t Tache %d : tourne sur le noeud (ti_host) : %d ; Commentaire TACHE PRINCIPALE \n", taskinfo[i].ti_tid, taskinfo[i].ti_host);
		} else {
			//printf("\t Tache daemon %d : tourne sur le noeud (ti_host) : %d ;\n", taskinfo[i].ti_tid, taskinfo[i].ti_host);
			iddaemon = taskinfo[i].ti_tid;
		}

	}
	
	info = pvm_config(&nhosttotal, &narch, &hostp);
        if (nhost == -1) {
		nhost = nhosttotal;
	} else if(nhost > nhosttotal) {
		printf("Not enough PVM hosts.\n");
		exit(EXIT_FAILURE);
	}

	//printf("Init paralléllisation\n");
	//printf("nhost : %d\nnarch : %d\n", nhost, narch);

	int f=0;
	int buffer[block_size];
	int buffer_receive[block_size];
	int new_buffer[block_size];
	int b=0;
	int host=0;
	int offset=0;
	int offset_receive=0;
	int z=0;
	x=0;
	int z2;
	//printf("\n\nInitialisation ... \n");
	while (host < nhost) {
		param[0] = calloc(1, MAX_CHAINE);
		param[1] = calloc(1, MAX_CHAINE);
		sprintf(param[0], "%d", host);
		sprintf(param[1], "%s", hostp[host].hi_name);
		param[2] = NULL;
		nbtaches = pvm_spawn(Fichier, &param[0], PvmTaskHost, hostp[host].hi_name,1, &numtaches[host]);
		msgtype = MAITRE_ENVOI;
		offset=host*block_size;

		for (f=0 ; f < block_size ; f++) {
		        z = f+offset;
			x = z % X;
			y = z / X;
			buffer[f] = image[y][x];
			
			//printf("(%d, %d)%d  ", x, y, image[y][x]);
		}
		
		pvm_initsend(PvmDataDefault);
		pvm_pkint(&mytid, 1, 1);
		pvm_pkfloat(&ETALEMENT, 1, 1);
		pvm_pkint(&LE_MIN, 1, 1);
		pvm_pkint(&offset, 1, 1);
		pvm_pkint(&block_size, 1, 1);
		//printf("buffer %d\n", buffer[block_size-1]);
		pvm_pkint(&buffer[0], block_size, 1);  // Send the line
		pvm_send(numtaches[host], msgtype);
		
		host += 1;
	}
	
	
	//printf("Début paralléllisation\n");
	char nameEsclave[MAX_CHAINE];
	x=0;
	y=0;
	host=0;
	int x2=0, y2=0;
	int xIdx=0, yIdx=0;
	const int total_buffers= ceil(X * Y / block_size);
	int new_offset=0;
	float coef_progress=(float)100/TailleImage;
	double execution_t;
	 while (x < X && y < Y) {
		//printf("\n\nReception ... \n");
		msgtype = MAITRE_RECOIT;
		pvm_recv(-1, msgtype);
		pvm_upkint(&tidEsclave, 1, 1);
		pvm_upkstr(&nameEsclave[0]);
		pvm_upkint(&offset_receive, 1, 1);
		pvm_upkint(&buffer_receive[0], block_size, 1);
		pvm_upkdouble(&execution_t, 1, 1);
		total_calculating_t += execution_t;
		//printf("spent time: %f (+%f)\n", total_calculating_t, execution_t);
		new_offset = offset_receive + block_size;
		
		for (f=0 ; f < block_size ; f++) {
			// Charge les données recues
			z = f+offset_receive;
			x = z % X;
			y = z / X;
			if (x < X && y < Y) {
				resultat[y][x] = buffer_receive[f];
			}
		
			// Provisionne les données à envoyer
			z2 = z+block_size;
			x2 = z2 % X;
			y2 = z2 / X;
			//printf("\n\n(%d, %d)\n", x2, y2);
			if (x2 < X && y2 < Y) {
				new_buffer[f] = image[y2][x2];
			}
			
			//printf("%d  ", buffer_receive[f]);
			//printf("%d  ", new_buffer[f]);
		}

		// Trouve index de l'hote esclave
		host = name2idx(nameEsclave, nhost, hostp);
		
		//printf("progress: %f%%", (coef_progress*(y*block_size+x)) );
	
		// Envoi
		//printf("\n\nEnvoi (host %d) ... \n", host);
		/*param[0] = calloc(1, MAX_CHAINE);
		param[1] = calloc(1, MAX_CHAINE);*/
		sprintf(param[0],"%d", host);
		sprintf(param[1], "%s", hostp[host].hi_name); 
		param[2] = NULL;
		nbtaches = pvm_spawn(Fichier, &param[0], PvmTaskHost, hostp[host].hi_name,1, &numtaches[host]);
		msgtype = MAITRE_ENVOI;
		pvm_initsend(PvmDataDefault);
		pvm_pkint(&mytid, 1, 1);
		pvm_pkfloat(&ETALEMENT, 1, 1);
		pvm_pkint(&LE_MIN, 1, 1);
		pvm_pkint(&new_offset, 1, 1);
		pvm_pkint(&block_size, 1, 1);
		pvm_pkint(&new_buffer[0], block_size, 1);
		pvm_send(numtaches[host], msgtype);
	}
	//printf("\n\nFin du LanceTaches ... \n");
	
	pvm_exit();

	/*========================================================================*/
	/* Sauvegarde de l'image dans le fichier resultat			*/
	/*========================================================================*/
	
	n = 0;
	for (i = 0 ; i < Y ; i++) {
		for (j = 0 ; j < X ; j++) {
			
			//fprintf(Dst,"%3d ",image[i][j]);
			fprintf(Dst,"%d ",resultat[i][j]);
			n++;
			if (n == NBPOINTSPARLIGNES) {
				n = 0;
				fprintf(Dst, "\n");
			}
		}
	}
				
	fprintf(Dst,"\n");
	fclose(Dst);

	//fprintf(TimeCSV, "%f;", total_calculating_t);
	//fclose(TimeCSV);
	
	//printf("\n");

	clock_gettime(CLOCK_REALTIME, &end_global_t);
	// time in nanoseconds
	total_global_t = (end_global_t.tv_sec - start_global_t.tv_sec) * 1000000000
	+ (end_global_t.tv_nsec - start_global_t.tv_nsec);

	printf("{ \"calculating_time\": %f, \"global_time\":%f}", total_calculating_t, total_global_t);
	
	exit(0); 
}


