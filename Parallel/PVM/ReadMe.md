# Quick start
Compile ".c" file:
```bash
make rebuild
```

Use "LanceTaches":
```bash
./LanceTaches -f images/image1.pgm -b <buffer_size> -m <machine_count>
```

Setup virtualenv before using "timing.py"
```bash
virtualenv env -p python3
source env/bin/activate
pip install -r requirements.txt
```

Use "timing.py":
```bash
python timing.py -e <number_of_executions> -f <file_compiled_to_execute> -a <args_to_pass>
# python timing.py -e100 -f "LanceTaches" -a "-f images/stavrovouni.pgm -b 5000"
```

Deactivate the virtualenv:
```bash
deactivate
```

