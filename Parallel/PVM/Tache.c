/*==============================================================================*/
/* Programme 	: Tache.c (Escalve)						*/
/* Auteur 	: Daniel CHILLET						*/
/* Date 	: Novermbre 2001						*(
/* 										*/
/* Objectifs	: Une tache qui fait pas grand chose d'autre que d'afficher	*/
/* 		  sur quelle machine elle tourne 				*/
/* 										*/
/* Principe	: Tache est lance par le maitre (LanceTaches) 			*/
/* 										*/
/*==============================================================================*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include "pvm3.h"

#define FOPEN(fich,fichier,sens) 	if ((fich=fopen(fichier,sens)) == NULL) { 				\
						printf("Probleme d'ouverture du fichier %s\n",fichier);		\
						exit(-1);							\
					} 

#define CALLOC(ptr, nr, type) 		if (!(ptr = (type *) calloc((size_t)(nr), sizeof(type)))) {		\
						printf("Erreur lors de l'allocation memoire \n") ; 		\
						exit (-1);							\
					} 
					
#define MAX_CHAINE 100


#define MAITRE_ENVOI 	0
#define MAITRE_RECOIT	5

#define ESCLAVE_ENVOI	MAITRE_RECOIT
#define ESCLAVE_RECOIT 	MAITRE_ENVOI

main(argc, argv)
int argc;
char *argv[];
{
	int block_size;
	int *ligne;
	float ETALEMENT;
	int LE_MIN;
	int offset;
	int y;
	
	int i = 0;
	int posi;
	int param;
	//int indice1 = -1;
	//int indice2 = -1;
	int mytid;
	int nbtaches;
	//int info1;
	//int info2;
	//int ti_host;
	int nhost;
	int narch;
	int maitre;
	int msgtype;
	int newvaleur;
	char machine[MAX_CHAINE];
	//char fichierOutput[MAX_CHAINE];
	struct pvmtaskinfo *taskinfo;
	struct pvmhostinfo *hostp;

	struct timespec start_t, end_t;
	double total_t;
	
	//FILE *OutputFile;

	wait(2);
	
	sscanf(argv[1],"%d",&param);
	sscanf(argv[2],"%s",machine);
	
	taskinfo = calloc(1, sizeof(struct pvmtaskinfo));
	hostp = calloc(1, sizeof(struct pvmhostinfo));

	mytid = pvm_mytid();
	
	// Stores time seconds 
	time_t seconds; 
	time(&seconds); 
	//sprintf(fichierOutput, "/dev/null");
	//sprintf(fichierOutput, "/users/imr/ksalaun/tmp/Output_%ld_%d.txt", seconds, mytid);

	//FOPEN(OutputFile, fichierOutput, "w");
	//fprintf(OutputFile, "Début\n");
	
	msgtype = ESCLAVE_RECOIT;
	pvm_recv(-1 , msgtype);
	
	pvm_upkint(&maitre, 1, 1);
	pvm_upkfloat(&ETALEMENT, 1, 1);
	pvm_upkint(&LE_MIN, 1, 1);
	pvm_upkint(&offset, 1, 1);
	pvm_upkint(&block_size, 1, 1);
	
	CALLOC(ligne, block_size, int);
	
	pvm_upkint(&ligne[0], block_size, 1);

/*========================================================================*/
	/* Calcul de chaque nouvelle valeur de pixel							*/
	/*========================================================================*/
	
	//fprintf(OutputFile, "before clocking\n");
	//fflush(OutputFile);
	clock_gettime(CLOCK_REALTIME, &start_t);
	//int z,q; for (z=0,q=0; z<10000000; z++) { q+=z*z; } // load generator
	for (i = 0 ; i < block_size ; i++) {
		ligne[i] = ((ligne[i] - LE_MIN) * ETALEMENT);
	}
	clock_gettime(CLOCK_REALTIME, &end_t);
	// time in nanoseconds
	total_t = (end_t.tv_sec - start_t.tv_sec) * 1000000000
		+ (end_t.tv_nsec - start_t.tv_nsec);

	msgtype = ESCLAVE_ENVOI;
	
	pvm_initsend(PvmDataDefault);
	
	//pvm_pkint(&posi, 1, 1);
	pvm_pkint(&mytid, 1, 1);
	pvm_pkstr(&machine[0]);
	pvm_pkint(&offset, 1, 1);
	pvm_pkint(&ligne[0], block_size, 1);
	pvm_pkdouble(&total_t, 1, 1);
	
	pvm_send(maitre, msgtype);
	
	////fprintf(OutputFile, "Done \n");

	//fclose(OutputFile);
	
	pvm_exit(); 
	exit(0); 

}
