# Quick start
Compile ".c" file:
```bash
make rebuild
```

Run "Cuda":
```bash
./Cuda -f "images/image1.pgm"
```

Setup virtualenv before using "timing.py":
```bash
virtualenv env -p python3
source env/bin/activate
pip install -r requirements.txt
```

Use "timing.py":
```bash
python timing.py -e <number_of_executions> -f <file_compiled_to_execute> -a <args_to_pass>
# python timing.py -e 100 -f "Cuda" -a "images/image1.pgm"
```

Deactivate the virtualenv:
```bash
deactivate
```

