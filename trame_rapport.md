Votre rapport doit contenir :
  * une explication de la façon dont vous avez parallélisé votre code pour les différents modèles de parallélisme utilisés
  * des résultats de mesures de temps d'exécution avec précision des configurations utilisées (vos résultats doivent être reproductibles)
    * pour générer des résultats facilement, il vous sera utile de créer des scripts (en perl, php, shell) de façon à lancer un grand nombre de simulations avec des configurations différentes
    * veillez à récupérer des valeurs moyenne de temps d'exécution en réalisant un nombre important de simulations pour chaque simulation, calculez la moyenne du temps d'exécution
  * ces résultats doivent être comparés à l'exécution séquentielle
  * vous devez préciser ce que vous mesurez
    * notamment pour Cuda, est ce que vous mesurez les temps de transfert mémoire CPU à mémoire GPU ?
    * pour la simulation Cuda, effectuez des mesures des temps de copies du CPU vers le GPU, et vice-versa
  * une analyse critique de ces résultats au regard du parallélisme de la machine, du parallélisme de l'application (recherchez les informations sur le processeur, la carte graphique)
  * une conclusion sur le type de parallélisme le mieux adapté à cette application
  * une conclusion générale sur l'exploitation de tous les parallélismes possible dans le cadre de cette application
  * Lorsque cela vous parait utile, vous pouvez placer des bouts de code dans le rapport (à condition que cela apporte quelque chose dans la compréhension de vos explications)

