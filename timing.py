#!/usr/bin/env python
# coding: utf-8

import os
import sys
import time
import json
import matplotlib.pyplot as plt
import subprocess
from subprocess import Popen
from optparse import OptionParser


execution_count = 3 #500
machine_count = 10
scriptCodeSequentiel = "CodeSequentiel/CodeSequentiel"
scriptPVM = "Parallel/PVM/LanceTaches"
scriptOpenMp = "Parallel/OpenMP/OpenMp"
scriptCuda = "Parallel/CUDA/Cuda"

x_list = []

average_sequential_time = {
  "small": [],
  "medium": [],
  "large": [],
}

images = {
  "small": { "file": "images/image1.pgm", "weight": 48841}, #221 x 221
  "medium": { "file": "images/MontagneFoncee.pgm", "weight": 307200}, #640 × 480
  "large": { "file": "images/stavrovouni.pgm", "weight": 15075424}, #5177 x 2912
}


def initSequentialTime():
  global execution_count, x_list, average_sequential_time, images

  # collect execution time
  x_list = range(1, execution_count+1)
  for img in ["small", "medium", "large"]:
    custom_args = "-f %s" % images[img]["file"]
    (res_execution_time, res_execution_time_average, res_calculating_time, res_calculating_time_average) = execution(scriptCodeSequentiel, custom_args)
    average_sequential_time[img] = [res_execution_time_average] * execution_count
    diff_ms = res_execution_time_average - res_calculating_time_average
    diff_percent = (100 / res_execution_time_average) * diff_ms

    # show statement
    print("Sequential values for %s:" % img)
    print("Execution time average (ms): %f" % res_execution_time_average)
    print("computing time average (ms): %f" % res_calculating_time_average)
    print("Difference (ms): %f" % diff_ms)
    print("Difference (%%): %f \n" % diff_percent)


def execution(filename, args=""):
  res_execution_time = []
  res_execution_time_average = 0
  res_calculating_time = []
  res_calculating_time_average = 0

  for i in range(0, execution_count):
    launcher = "./%s" % filename
    p = subprocess.Popen([launcher, args], universal_newlines=True, stdout=subprocess.PIPE)
    output, errors = p.communicate()
    try:
      output = json.loads(output)
      calculating_time = output["calculating_time"] / 1000000 #ns to ms
      res_calculating_time.append(calculating_time)
      res_calculating_time_average += (calculating_time / execution_count)

      global_time = output["global_time"] / 1000000 #ns to ms
      res_execution_time.append(global_time)
      res_execution_time_average += (global_time / execution_count)
    except Exception, e:
      print(e)
      sys.exit(output)

  return (res_execution_time, res_execution_time_average, res_calculating_time, res_calculating_time_average)


def statement(title, filename):
  global x_list, execution_count, average_sequential_time, images
  y_list_computing_time = { "small": [], "medium": [], "large": [] }
  y_list_execution_time = { "small": [], "medium": [], "large": [] }
  y_list_average_execution_time = { "small": [], "medium": [], "large": [] }

  for img in ["small", "medium", "large"]:
    # collect execution time
    custom_args = "-f %s" % images[img]["file"]
    (res_execution_time, res_execution_time_average, res_calculating_time, res_calculating_time_average) = execution(filename, custom_args)
    y_list_computing_time[img] = res_calculating_time
    y_list_execution_time[img] = res_execution_time
    y_list_average_execution_time[img] = [res_execution_time_average] * execution_count
    diff_ms = res_execution_time_average - res_calculating_time_average
    diff_percent = (100 / res_execution_time_average) * diff_ms
  
    # show statement
    print("%s values for %s:" % (title, img))
    print("Execution time average (ms): %f" % res_execution_time_average)
    print("Computing time average (ms): %f" % res_calculating_time_average)
    print("Difference (ms): %f" % diff_ms)
    print("Difference (%%): %f \n" % diff_percent)

  for img in ["small", "medium", "large"]:
    # generate the graph
    try:
      plt.title("%s statement for %s" % (title, img))
      plt.plot(x_list, average_sequential_time[img], "-r")
      plt.plot(x_list, y_list_average_execution_time[img], "-b")
      plt.plot(x_list, y_list_computing_time[img], "-g")
      plt.plot(x_list, y_list_execution_time[img], "-m")
      plt.legend(['average sequential execution time', 'average parallel execution time', 'parallel computing time', 'parallel execution time'], loc='upper left')
      plt.show()
    except:
      pass
    print("##### DATA TO RECOVER #####")
    print("%s statement for %s" % (title, img))
    print("X axes: %s" % x_list)
    print("Y axes:")
    print("average_sequential_time: %s" % average_sequential_time[img])
    print("average_execution_time: %s" % y_list_average_execution_time[img])
    print("computing_time: %s" % y_list_computing_time[img])
    print("execution_time: %s" % y_list_execution_time[img])
    print("###########################")


def specialScale(min, max):
  res = []
  x = min
  while x < max:
    res.append(x)
    x *= 4
  return res


#############################################


def pvmEvolutionByMachine():
  global machine_count, images
  y_list = []
  x_list = range(1, machine_count+1)

  # collect execution time
  for i in x_list:
    custom_args = "-f %s -m %i" % (images["large"]["file"], i)
    (res_execution_time, res_execution_time_average, res_calculating_time, res_calculating_time_average) = execution(scriptPVM, custom_args)
    y_list.append(res_execution_time_average)
    print("intermediate execution time average for %i machines: %f" % (i, res_execution_time_average))
    print("intermediate execution time: %s" % res_execution_time)

  # generate the graph
  try:
    plt.title("PVM Evolution By Machine")
    plt.plot(x_list, y_list, "-m")
    plt.show()
  except:
    pass
  print("##### DATA TO RECOVER #####")
  print("PVM Evolution By Machine")
  print("X axes: %s" % x_list)
  print("Y axes: %s" % y_list)
  print("###########################")
  

def pvmEvolutionByBufferSize():
  global images
  picture_size = images["large"]["weight"]
  y_list = []
  #x_list = range(1000, picture_size+1, 1000)
  x_list = specialScale(1000, picture_size)
  
  # collect execution time
  for i in x_list:
    custom_args = "-f %s -b %i" % (images["large"]["file"], i)
    (res_execution_time, res_execution_time_average, res_calculating_time, res_calculating_time_average) = execution(scriptPVM, custom_args)
    y_list.append(res_execution_time_average)
    print("intermediate execution time average for buffer of %i: %f" % (i, res_execution_time_average))
    print("intermediate execution time: %s" % res_execution_time)

  # generate the graph
  try:
    plt.title("PVM Evolution By Buffer Size")
    plt.plot(x_list, y_list, "-m")
    plt.show()
  except:
    pass
  print("##### DATA TO RECOVER #####")
  print("PVM Evolution By Buffer Size")
  print("X axes: %s" % x_list)
  print("Y axes: %s" % y_list)
  print("###########################")


def pvmStatement():
  statement("PVM", scriptPVM)


def cudaEvolutionByBufferSize():
  global images
  picture_size = images["large"]["weight"]
  y_list = []
  x_list = premiers(picture_size)
  
  # collect execution time
  for i in x_list:
    custom_args = "-f %s -b %i" % (images["large"]["file"], i)
    (res_execution_time, res_execution_time_average, res_calculating_time, res_calculating_time_average) = execution(scriptCuda, custom_args)
    y_list.append(res_execution_time_average)

  # generate the graph
  try:
    plt.title("PVM Evolution By Buffer Size")
    plt.plot(x_list, y_list, "-m")
    plt.show()
  except:
    pass
  print("##### DATA TO RECOVER #####")
  print("PVM Evolution By Buffer Size")
  print("X axes: %s" % x_list)
  print("Y axes: %s" % y_list)
  print("###########################")


def cudaStatement():
  statement("CUDA", scriptCuda)


def openMpEvolutionByThreadCount():
  global images
  y_list = []
  x_list = range(2, 64, 8)
  
  # collect execution time
  for i in x_list:
    custom_args = "-f %s -t %i" % (images["large"]["file"], i)
    (res_execution_time, res_execution_time_average, res_calculating_time, res_calculating_time_average) = execution(scriptOpenMp, custom_args)
    y_list.append(res_execution_time_average)
    print("intermediate execution time average for %i threads: %f" % (i, res_execution_time_average))

  # generate the graph
  try:
    plt.title("OpenMp Evolution By Thread Count")
    plt.plot(x_list, y_list, "-m")
    plt.show()
  except:
    pass
  print("##### DATA TO RECOVER #####")
  print("OpenMp Evolution By Thread Count")
  print("X axes: %s" % x_list)
  print("Y axes: %s" % y_list)
  print("###########################")


def openMpStatement():
  statement("OpenMp", scriptOpenMp)


#######################################################


def exit():
    sys.exit()


def exec_menu(choice):
    os.system('clear')
    ch = choice.lower()
    if ch == '':
        menu_actions['mainMenu']()
    else:
        try:
            menu_actions[ch]()
        except KeyError:
            menu_actions['mainMenu'](True)


def mainMenu(error=False):
    os.system('clear')

    if error:
      print("Invalid selection, please try again.\n")
    else:
      print("Welcome,\n")
    print("Please choose the menu you want to start:")
    print("1. pvmEvolutionByMachine")
    print("2. pvmEvolutionByBufferSize")
    print("3. pvmStatement")
    print("4. cudaEvolutionByBufferSize")
    print("5. cudaStatement")
    print("6. openMpEvolutionByThreadCount")
    print("7. openMpStatement")
    print("\n0. Quit")
    choice = raw_input(" >>  ")
    exec_menu(choice)


menu_actions = {
  'mainMenu': mainMenu,
  '1': pvmEvolutionByMachine,
  '2': pvmEvolutionByBufferSize,
  '3': pvmStatement,
  '4': cudaEvolutionByBufferSize,
  '5': cudaStatement,
  '6': openMpEvolutionByThreadCount,
  '7': openMpStatement,
  '0': exit,
}


def premiers(n):
    """Itérateur retourne tous les nb premiers <= n (crible d'Eratosthene)
       on utilise ici le module 'bitarray' pour stocker les booléens
    """
    if n<2:
        pass # il n'y a aucun nb 1er en dessous de 2!
    else:
        n += 1 # pour avoir les nb 1ers <=n et pas seulement <n
        tableau = bitarray(n)
        tableau.setall(True)
        tableau[0], tableau[1] = False, False # 1 n'est pas un nb 1er
        yield 2  # 2 est un nombre premier
        tableau[2::2] = False # on élimine tous les nombres pairs
        racine = int(n**0.5)
        racine = racine + [1,0][racine%2] # pour que racine de n soit impair
        i, fin, pas = 3, racine+1, 2
        while i<fin: # on ne traite que les nb impairs
            if tableau[i]:
                yield i # on a trouvé un nouveau nb 1er: on le renvoie
                tableau[i::i] = False # on élimine i et ses multiples
            i += pas
        i, fin, pas = racine, n, 2
        while i<fin:  # on ne traite que les nb impairs
            if tableau[i]:
                yield i # on a trouvé un nouveau nb 1er: on le renvoie
            i += pas


#######################################################


def main(argv):
  global execution_count, machine_count, average_sequential_time, x_list
  parser = OptionParser()
  parser.add_option("-e", "--execution-count", dest="execution_count",
    type="int", help="number of executions", metavar="COUNT")
  parser.add_option("-m", "--machine-count", dest="machine_count",
    type="int", help="number of machines", metavar="COUNT")

  (options, args) = parser.parse_args()
  if options.execution_count != None:
    execution_count = options.execution_count
  if options.machine_count != None:
    machine_count = options.machine_count

  # Execution
  #initSequentialTime()
  x_list = range(1, execution_count+1)
  average_sequential_time["small"] = [32.487] * execution_count,
  average_sequential_time["medium"] = [154.804] * execution_count,
  average_sequential_time["large"] = [3502.444] * execution_count,
  while (True):
  	mainMenu()


if __name__ == "__main__":
  main(sys.argv[1:])

